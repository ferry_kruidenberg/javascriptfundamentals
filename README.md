# JavaScriptFundamentals - A simple dynamic webpage using vanilla JavaScript

In this simple webpage the user is able to buy laptops. In order to do so he needs to have sufficient balance. 
To get this he can work (pressing the button) and transfer it to the bank. Another way to get money is to request a loan of not more than twice its balance. 
Once enough money is acquired he chooses his favorite laptop and buys it.

