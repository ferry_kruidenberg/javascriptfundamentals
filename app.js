import {BankHelper} from './helpers/bankHelper.js'
import {WorkHelper} from './helpers/workHelper.js'
import {LaptopHelper} from "./helpers/laptopHelper.js";


async function init() {
    const bankHelper = new BankHelper()
    const workHelper = new WorkHelper()
    const laptopHelper = new LaptopHelper()

    bankHelper.init(workHelper)
    workHelper.init(bankHelper)
    await laptopHelper.init(bankHelper)
}

init();



