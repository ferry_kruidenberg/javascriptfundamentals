//Bank elements
const elBalance = document.getElementById("balance")
const elGetLoanButton = document.getElementById("loanButton")
const elLoanBalance = document.getElementById("loanBalance")
const elLoanBalanceField = document.getElementById("loanBalanceField")

//Form elements
const elLoanForm = document.getElementById("popupForm")
const elLoanFormCancelButton = document.getElementById("loanCancel")
const elLoanFormSubmitButton = document.getElementById("LoanSubmit")
const elLoanInputField = document.getElementById("elLoanInputField")


//The BankHelper class
//This class handles all the interactions with bank
//and the form to request the loan
export class BankHelper{
    constructor() {
        this.balance = 0.0
        this.loan = 0.0

        elBalance.innerText = this.balance.toFixed(2) + ' Kr'
        elLoanBalanceField.innerText = this.loan.toFixed(2) + ' Kr'

        const self = this

        //Form Listeners
        elLoanFormCancelButton.addEventListener('click', function () {
            elLoanForm.style.display = "none"
        })

        elLoanFormSubmitButton.addEventListener('click', function (){
            self.getLoan()
        })

        elGetLoanButton.addEventListener('click', function () {
            if (self.loan > 0.0) {
                alert("You have already a loan!")
            } else {
                elLoanForm.style.display = "block"
                elLoanInputField.focus();
                elLoanInputField.select();
            }
        })

        window.addEventListener('keydown', function (e) {
            if (e.keyIdentifier === 'U+000A' || e.keyIdentifier === 'Enter' || e.keyCode === 13) {
                if (e.target.nodeName === 'INPUT' && e.target.type !== 'textarea'&& e.target.id==='elLoanInputField') {
                    e.preventDefault();
                    self.getLoan()
                    return false;
                }
            }
        }, true);
    }

    init(workHelper) {
        this.workHelper = workHelper
    }

    getLoan() {
        if (elLoanInputField.value > 2 * this.balance) {
            alert("You can't loan more than twice your balance.")
        } else if (Number(elLoanInputField.value) > 0){
            this.loan = Number(Number(elLoanInputField.value).toFixed(2))
            this.balance += this.loan
            elBalance.innerText = this.balance.toFixed(2) + ' Kr'
            elLoanForm.style.display = "none"
            elLoanBalance.style.display = "block"
            elLoanBalanceField.innerText = this.loan.toFixed(2) + ' Kr'
            this.workHelper.showHideRepaybutton()
        } else {
            alert("Input an amount greater than zero.")
        }
    }

    updateLoan(value){
        this.loan = value
        elLoanBalanceField.innerText = this.loan.toFixed(2) + ' Kr'
        if (this.loan > 0.0){
            elLoanBalance.style.display = "block"
        }
        else {
            elLoanBalance.style.display = "none"
        }
        this.workHelper.showHideRepaybutton()
    }

    updateBalance(value){
        this.balance = value
        elBalance.innerText = this.balance.toFixed(2) + ' Kr'
    }


}

