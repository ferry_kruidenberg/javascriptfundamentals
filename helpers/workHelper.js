//Work elements
const elWorkButton = document.getElementById("workButton")
const elPay = document.getElementById("pay")
const elBankButton = document.getElementById('bankButton')
const elRepayLoanButton = document.getElementById("repayLoanButton")


//This is the WorkHelper class
//This class handles all the interactions with work
export class WorkHelper {
    constructor() {
        this.pay = 0.0

        const self = this

        elWorkButton.addEventListener('click', function () {
            self.work()
        })

        elBankButton.addEventListener('click', function () {
            self.transferMoneyToBank()
        })

        elRepayLoanButton.addEventListener('click', function () {
            self.repayLoan()
        })
    }
    
    init(bankHelper) {
        this.bankHelper = bankHelper
    }

    work() {
        this.pay += 100
        elPay.innerText = this.pay.toFixed(2) + ' Kr'
    }

    transferMoneyToBank() {
        if (this.pay / 10 < this.bankHelper.loan) {
            this.bankHelper.updateLoan(this.bankHelper.loan - this.pay / 10)
            this.bankHelper.updateBalance(this.bankHelper.balance + 9 * this.pay / 10)
        }
        else {
            this.bankHelper.updateBalance(this.bankHelper.balance + this.pay - this.bankHelper.loan)
            this.bankHelper.updateLoan(0.0)
        }

        this.pay = 0.0
        elPay.innerText = this.pay.toFixed(2) + ' Kr'
    }

    repayLoan() {
        if (this.pay < this.bankHelper.loan) {
            this.bankHelper.updateLoan(this.bankHelper.loan - this.pay)
            this.pay = 0.0
        } else {
            this.pay -= this.bankHelper.loan
            this.bankHelper.updateLoan(0.0)
        }

        elPay.innerText = this.pay.toFixed(2) + ' Kr'
    }

    showHideRepaybutton(){
        if (this.bankHelper.loan > 0){
            elRepayLoanButton.style.display = "block"
        }
        else {
            elRepayLoanButton.style.display = "none"
        }
    }
}