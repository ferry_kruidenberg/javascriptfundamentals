//Laptops elements
const elLaptopSelectionBox = document.getElementById('laptopsSelectionBox')
const elFeaturesLu = document.getElementById("features")
const elLaptopName = document.getElementById("laptopName")
const elLaptopDescription = document.getElementById("laptopDescription")
const elLaptopImage = document.getElementById("laptopImage")
const elLaptopPrice = document.getElementById("laptopPrice")
const elLaptopBuyButton = document.getElementById("buyButton")

//This is the LaptopHelper class
//This handles all the interactions for the laptop
//This includes the api-calls to get the data and images of the laptops
export class LaptopHelper {
    constructor() {
        this.computers = [];
        this.urlBase = "https://noroff-komputer-store-api.herokuapp.com/"

        const self = this
        elLaptopSelectionBox.addEventListener('change', function () {
            self.updateLaptop()
        })

        elLaptopBuyButton.addEventListener('click', function () {
            self.buyLaptop()
        })

        elLaptopImage.addEventListener('error', function () {
            const loc = window.location.pathname;
            const dir = loc.substring(0, loc.lastIndexOf('/'));
            elLaptopImage.src = dir + "/placeholder.jpg"
        })
    }

    async init(workHelper) {
        this.workHelper = workHelper
        try {
            this.computers = await this.fetchLaptops();
            this.populateLaptop()
            this.updateLaptop()
        } catch (error) {
            console.log('ERROR!', error.message);
        }
    }


    fetchLaptops() {
        return fetch(this.urlBase + 'computers')
            .then(function (response) {
                if (!response.ok) {
                    throw new Error('Could not fetch the posts. 😭');
                }
                return response.json();
            })
            .then(function (laptops) {
                return laptops
            })
    }

    populateLaptop() {
        if (this.computers.length === 0) {
            elLaptopSelectionBox.innerHTML = "<option></option>"
        } else {
            let catOptions = "";
            for (const computer of this.computers) {
                catOptions += "<option>" + computer["title"] + "</option>";
            }
            elLaptopSelectionBox.innerHTML = catOptions;
        }
    }

    setFeatures() {
        elFeaturesLu.innerHTML = ""
        let features = this.computers[elLaptopSelectionBox.selectedIndex]["specs"]
        for (const feature of features) {
            const elFeatureItem = document.createElement('li');
            elFeatureItem.innerText = feature
            elFeaturesLu.appendChild(elFeatureItem)
        }
        elLaptopImage.src = this.urlBase + this.computers[elLaptopSelectionBox.selectedIndex]["image"]
    }

    updateLaptop() {
        this.setFeatures()
        elLaptopName.innerText = this.computers[elLaptopSelectionBox.selectedIndex]["title"]
        elLaptopDescription.innerText = this.computers[elLaptopSelectionBox.selectedIndex]["description"]
        elLaptopPrice.innerText = this.computers[elLaptopSelectionBox.selectedIndex]["price"] + " NOK"
    }

    buyLaptop() {
        if (this.workHelper.balance < this.computers[elLaptopSelectionBox.selectedIndex]["price"]) {
            alert("Your balance is not sufficient, work more!!!")
        } else {
            this.workHelper.updateBalance(this.workHelper.balance -
                this.computers[elLaptopSelectionBox.selectedIndex]["price"])
            alert(`Congratulations you are the proud owner of a new ${this.computers[elLaptopSelectionBox.selectedIndex]["title"]}!!!`)
        }
    }
}